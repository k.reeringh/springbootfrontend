package com.company;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Scanner;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONArray;

public class Main {

    public static void main(String[] args) throws IOException {
	// write your code here

        URL url = new URL("http://localhost:8080/blog");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.connect();
        int responseCode = con.getResponseCode();

        if(responseCode != 200){
            throw new RuntimeException("HttpResponseCode" + responseCode);
        }else{
            StringBuilder informationString = new StringBuilder();
            Scanner scanner = new Scanner(url.openStream());

            while(scanner.hasNext()){
                informationString.append(scanner.nextLine());
            }
            scanner.close();
            System.out.println(informationString);

        }


    }
}
